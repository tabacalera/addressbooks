import React from "react";
import { Provider } from "react-redux";
import { createStore, combineReducers, applyMiddleware } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";
import storage from "redux-persist/lib/storage";

import thunk from "redux-thunk";
// import ItemList from "./app/pages/item/ItemList"
// import items from "./app/pages/item/Item.reducers"
import addressBooks from "./app/pages/AddressBook/AddressBook.reducers";
import reports from "./app/pages/AddressBook/Reports/AddressBookReports.reducers";
import authReducer from "./app/auth/Auth.reducers";

import AddressBookForm from "./app/pages/AddressBook/AddressBookForm";
import AddressBookList from "./app/pages/AddressBook/AddressBookList";
import AddressBookEdit from "./app/pages/AddressBook/AddressBookEdit";

import AddressBookReportsList from "./app/pages/AddressBook/Reports/AddressBookReportsList";

import LoginPage from "./app/auth/LoginPage";
import RegisterPage from "./app/auth/RegisterPage";
import ResetPage from "./app/auth/ResetPage";
import ChangePassword from "./app/auth/ChangePassword";
import PrivateRoute from "./app/auth/PrivateRoute";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";


const persistConfig = {
  key: "root",
  storage,
};
const reducers = combineReducers({ addressBooks, reports, authReducer });

const persistedReducer = persistReducer(persistConfig, reducers);
let store = createStore(persistedReducer, applyMiddleware(thunk));
let persistor = persistStore(store);

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router>
          <Switch>
            <Route path="/" exact component={LoginPage} />
            <Route path="/register" component={RegisterPage} />
            <Route path="/reset" component={ResetPage} />
            <PrivateRoute
              exact
              path="/user/change"
              component={ChangePassword}
            />
            <PrivateRoute exact path="/entry" component={AddressBookList} />
            <PrivateRoute
              exact
              path="/entry/edit"
              component={AddressBookEdit}
            />
            <PrivateRoute
              exact
              path="/entry/create"
              component={AddressBookForm}
            />
            <PrivateRoute
              exact
              path="/reports"
              component={AddressBookReportsList}
            />
          </Switch>
        </Router>
      </PersistGate>
    </Provider>
  );
};

export default App;
