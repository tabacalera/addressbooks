import { AddressBook, AddressBookReport } from "../../domain/entities/AddressBook";
import { AddressBookRepository } from "../../domain/repositories/AddressBookRepository";
import {firebaseApp, storage } from "./firestore"


var db = firebaseApp.firestore();


class AddressBookDTO {
    id: number=0;
    firstName: string="";
    lastName: string="";
    middleName: string="";
    birthday: string="";
    age: number=0;
    gender: string="";
    image?: any;
}
class AddressBookReportDTO {
    male: number=0;
    female: number=0;
    others: number=0;
}
export class AddressBookRepositoryFireBaseImpl implements AddressBookRepository {

    async GetAddressBookReports(): Promise<AddressBookReport[]> {
        let addressBookReportArr: any = []
        let dataRef = db.collection("totals")
        var active = await dataRef.get()
        
        const data = active.docs.map((doc: any)=>  doc.data())
        const id = active.docs.map((doc: any)=>  doc.id)
        console.log(data)
        console.log(id)

        for (let i = 0; i < data.length; i++) {
            var jsonString = JSON.stringify(data[i])
            var res = JSON.parse(jsonString)
            addressBookReportArr.push(res)
        }
        for(let i =0; i < id.length; i++){
            addressBookReportArr[i].id = id[i]
        }
        console.log(addressBookReportArr);
        return addressBookReportArr.map((address: AddressBookReportDTO) => new AddressBookReport(address.male, address.female, address.others))
    }
    async GetAddressBook(): Promise<AddressBook[]> {
        let addressBookArr = []
        let dataRef = db.collection("addressbook")
        var active = await dataRef.get()
        
        const data = active.docs.map((doc: any)=>  doc.data())
        const id = active.docs.map((doc: any)=>  doc.id)
        

        for (let i = 0; i < data.length; i++) {
            var jsonString = JSON.stringify(data[i])
            var res = JSON.parse(jsonString)
            addressBookArr.push(res)
        }
        for(let i =0; i < id.length; i++){
            addressBookArr[i].id = id[i]
            addressBookArr[i].image = await storage.ref(`images`).child(addressBookArr[i].lastName).getDownloadURL().then(snapshot=> {return snapshot})
            
        }
        

        return addressBookArr.map((address: AddressBookDTO) => new AddressBook(address.id, address.firstName, address.lastName, address.middleName, address.birthday, address.age, address.gender, address.image));
    }
    async AddAddressBook(data:AddressBook) {
        console.log(data.image)
        var storageRef = storage.ref(`images/${data.lastName}`)
        storageRef.put(data.image).then(function(snapshot){
            console.log('Uploaded a blob or file', snapshot)
        })
        db.collection("addressbook").add({
            age:data.age,
            firstName: data.firstName,
            lastName: data.lastName,
            middleName: data.middleName,
            birthday: data.birthday,
            gender: data.gender
        })
        .then(()=>{
            console.log("Successfully added data")
        })
        .catch((err: any)=>{
            alert(err)
        })
    }
    async DeleteAddressBook(data:AddressBook) {
        console.log(data.id)
        const id = data.id.toString()
        db.collection("addressbook").doc(id).delete().then(()=>{
            console.log("successfully deleted")
        }).catch((err: any)=>{
            alert(err)
        })
    }
  
    async UpdateAddressBook(data:AddressBook) {
        console.log(data);
        const storageRef = storage.ref(`images/${data.lastName}`)
        if(typeof(data.image) !== 'string'){
            console.log("not",typeof(data.image));
            storageRef.put(data.image).then(function(snapshot){
                console.log(snapshot)
            })
        }else {
            console.log(typeof(data.image));

        }

        var addressbookRef = db.collection("addressbook").doc(data.id.toString())
        addressbookRef.set({
            age:data.age,
            firstName: data.firstName,
            lastName: data.lastName,
            middleName: data.middleName,
            birthday: data.birthday,
            gender: data.gender,
        })
        .then(()=>{
            console.log("Successfully added data")
        })
        .catch((err: any)=>{
            alert(err)
        })
    }
  }