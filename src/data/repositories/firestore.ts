import firebase from "firebase"

const firebaseConfig = {
    apiKey: "AIzaSyAjLBIBsX2um3scrWJaz8SORQuTIXYXUh0",
    authDomain: "addressbooks-61c8f.firebaseapp.com",
    databaseURL: "https://addressbooks-61c8f.firebaseio.com",
    projectId: "addressbooks-61c8f",
    storageBucket: "addressbooks-61c8f.appspot.com",
    messagingSenderId: "1742938575",
    appId: "1:1742938575:web:f6922a6c0fa9fb5589f2ba"
  };

const firebaseApp = firebase.initializeApp(firebaseConfig);
const storage = firebase.storage()
export {firebaseApp, storage}