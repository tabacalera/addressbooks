import { User, UserReset,currentUser, changePassUser } from "../../domain/entities/User";
import { AuthRepository } from "../../domain/repositories/AuthRepository";
import { firebaseApp } from "./firestore"
import firebase from "firebase"
export default class AuthRepositoryFirebaseImpl implements AuthRepository {
    async Signup(data:User) {
        await firebaseApp.auth().createUserWithEmailAndPassword(data.username, data.password)
        .then(function(){
            console.log("User Successfully signed up")
        })
        .catch(err=>{
            var errCode = err.code;
            var errMessage = err.message

            console.log(errCode+ " " +errMessage)
        })
    }

    async Login(data:User): Promise<currentUser>{ 
        await firebaseApp.auth().signInWithEmailAndPassword(data.username, data.password)
        .then(response=>{
            
        })
        .catch(err=>{
            var errCode = err.code;
            var errMessage = err.message

            console.log(errCode+ " " +errMessage)
        })
        var user:any = firebaseApp.auth().currentUser;
        
        return user
        // return user
    }

    async Logout() {
        
        await firebaseApp.auth().signOut()
        
        .then(res=>{
            console.log(res)
        })
        .catch(err=>{
            var errCode = err.code;
            var errMessage = err.message
            
            console.log(errCode+ " " +errMessage)
        })
    }
    async ResetPassword(data:UserReset) {
        const string = data.toString()
        console.log(string);
        await firebaseApp.auth().sendPasswordResetEmail(string).then(function(){
            return "success"
            
        })
        .catch(err=>{
            return "error"
        })
        
    }
    async ChangePassword(data:changePassUser): Promise<currentUser>{ 
        console.log(data)

        var user:any = firebaseApp.auth().currentUser;
        var credential = firebase.auth.EmailAuthProvider.credential(data.username,data.password)
        await user.reauthenticateWithCredential(credential)
        await user.updatePassword(data.newPassword)
        return user

    }
}