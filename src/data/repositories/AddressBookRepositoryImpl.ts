import { AddressBook, AddressBookReport } from "../../domain/entities/AddressBook";
import { AddressBookRepository } from "../../domain/repositories/AddressBookRepository";

class AddressBookDTO {
    id: number=0;
    firstName: string="";
    lastName: string="";
    middleName: string="";
    birthday: string="";
    age: number=0;
    gender: string="";
    image?: any;
}
class AddressBookReportDTO {
  male: number=0;
  female: number=0;
  others: number=0;
}
let addressBook = [{
    "id": 1,
    "firstName": "Jan",
    "lastName": "Ligutan",
    "middleName": "Borja",
    "birthday": "May 1, 1997",
    "age": 23,
    "gender": "male",
}]
export class AddressBookRepositoryImpl implements AddressBookRepository {
  async GetAddressBookReports(): Promise<AddressBookReport[]> {
    let addressBookReportArr: any = []
    return addressBookReportArr.map((address: AddressBookReportDTO) => new AddressBookReport(address.male, address.female, address.others))
}
  async GetAddressBook(): Promise<AddressBook[]> {
    var jsonString  = localStorage.getItem("addressBook")
    let objAddress
    if(jsonString!== null){
        objAddress = JSON.parse(jsonString)
    }else {
        localStorage.setItem("addressBook", JSON.stringify(addressBook))
    }
    return objAddress.map((address: AddressBookDTO) => new AddressBook(address.id, address.firstName, address.lastName, address.middleName, address.birthday, address.age, address.gender, address.image));
  }
  async AddAddressBook(data:AddressBook) {
    const jsonString = localStorage.getItem("addressBook")
    let objAddress
    if(jsonString!==null) {
      objAddress= JSON.parse(jsonString)
    }
    data.id += 1
    objAddress.push(data)
    var stringAddress = JSON.stringify(objAddress)
    localStorage.setItem("addressBook", stringAddress)
  }
  async DeleteAddressBook(data:AddressBook) {
    var intData = parseInt(data.id.toString())
    const jsonString = localStorage.getItem("addressBook")
    let objAddress
    if(jsonString!==null) {
      objAddress= JSON.parse(jsonString)
    }
    for (let i = 0; i < objAddress.length; i++) {
      if (intData === objAddress[i].id){
        objAddress.splice(i,1);
        break;
      }
    }
    var stringTodo = JSON.stringify(objAddress)
    localStorage.setItem("addressBook", stringTodo)
  }

  async UpdateAddressBook(data:AddressBook) {
    var intData = parseInt(data.id.toString())
    const jsonString = localStorage.getItem("addressBook")
    let objAddress
    if(jsonString!== null){
      objAddress = JSON.parse(jsonString)
    }
    for (let i = 0; i < objAddress.length; i++) {
      if (intData === objAddress[i].id){
        objAddress[i] = data
        break;
      }
    }
    var stringTodo = JSON.stringify(objAddress)
    localStorage.setItem("addressBook", stringTodo)
  }
}
