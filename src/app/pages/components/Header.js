import React from "react";
import { connect } from "react-redux";
import { Menu, Button } from "antd";
import { useHistory } from "react-router-dom";
import { logout } from "../../auth/Auth.actions";

const Header = ({ logout }) => {
  let history = useHistory();
  const [current, setCurrent] = React.useState("");
  const handleClick = (e) => {
    setCurrent({
      current: e.key,
    });
    if (e.key === "logout") {
      logout();
      history.replace("/");
    } else if (e.key === "change") {
      history.push("/user/change");
    } else {
      history.push(`/${e.key}`);
    }
  };
  return (
    <div>
      <Menu mode="horizontal" selectedKeys={[current]} theme="dark" onClick={handleClick}>
        <Menu.Item key="entry">Entry</Menu.Item>
        <Menu.Item key="reports">Reports</Menu.Item>
        <Menu.Item key="logout" style={{ float: "right" }}>
          Logout
        </Menu.Item>
        <Menu.Item key="change" style={{ float: "right" }}>
          Change Password
        </Menu.Item>
      </Menu>
    </div>
  );
};

const mapStateToProps = (state) => ({
  // auth: state.authReducer
});

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logout),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
