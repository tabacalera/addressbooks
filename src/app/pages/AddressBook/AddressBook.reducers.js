import {
  LIST_LOAD_REQUEST,
  LIST_LOAD_SUCCESS,
  LIST_LOAD_FAILURE,
  ADD_ADDRESS_BOOK,
  UPDATE_ADDRESS_BOOK,
} from "./AddressBook.types";

const initialState = {
  loading: false,
  addressBooks: [],
};

function addressBooks(state = initialState, action = null) {
  switch (action.type) {
    case LIST_LOAD_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case LIST_LOAD_FAILURE:
      return {
        ...state,
        loading: false,
      };

    case LIST_LOAD_SUCCESS:
      return {
        ...state,
        addressBooks: action.payload,
        loading: false,
      };
    case ADD_ADDRESS_BOOK:
      return Object.assign({}, state, {
        addressBooks: state.addressBooks.concat(action.payload),
        loading: false,
      });

    case UPDATE_ADDRESS_BOOK:
      return Object.assign({}, state, {
        addressBooks: state.addressBooks.concat(action.payload),
        loading: false,
      });

    default:
      return state;
  }
}

export default addressBooks;
