import {
  LIST_LOAD_REQUEST,
  LIST_LOAD_SUCCESS,
  LIST_LOAD_FAILURE,
  ADD_ADDRESS_BOOK,
  UPDATE_ADDRESS_BOOK,
} from "./AddressBook.types";
import { AddressBookServiceImpl } from "../../../domain/usecases/AddressBookService";
import { AddressBookRepositoryFireBaseImpl } from "../../../data/repositories/AddressBookRepositoryFireBaseImpl";

export const refreshList = async (dispatch) => {
  dispatch({ type: LIST_LOAD_REQUEST });

  try {
    const addressBookRepo = new AddressBookRepositoryFireBaseImpl();
    const addressBookService = new AddressBookServiceImpl(addressBookRepo);
    const addressBooks = await addressBookService.GetAddressBook();
    dispatch({ type: LIST_LOAD_SUCCESS, payload: addressBooks });
  } catch (error) {
    dispatch({ type: LIST_LOAD_FAILURE, error });
  }
};
export const addAddressBook = async (payload) => {
  try {
    const addressBookRepo = new AddressBookRepositoryFireBaseImpl();
    const addressBookService = new AddressBookServiceImpl(addressBookRepo);
    const addressBooks = await addressBookService.AddAddressBook(payload);
    return {
      type: ADD_ADDRESS_BOOK,
      payload: addressBooks,
    };
  } catch (error) {
    alert(error);
  }
};

export const deleteAddressBook = async (payload) => {
  try {
    const addressBookRepo = new AddressBookRepositoryFireBaseImpl();
    const addressBookService = new AddressBookServiceImpl(addressBookRepo);
    await addressBookService.DeleteAddressBook(payload);
  } catch (error) {
    alert(error);
  }
};

export const updateAddressBook = async (payload) => {
  try {
    const addressBookRepo = new AddressBookRepositoryFireBaseImpl();
    const addressBookService = new AddressBookServiceImpl(addressBookRepo);
    const addressBooks = await addressBookService.UpdateAddressBook(payload);
    return {
      type: UPDATE_ADDRESS_BOOK,
      payload: addressBooks,
    };
  } catch (error) {
    alert(error);
  }
};
