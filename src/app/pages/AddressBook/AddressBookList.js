import "./AddressBookList.css";
import React from "react";
import { connect } from "react-redux";
import {
  refreshList,
  deleteAddressBook,
  updateAddressBook,
} from "./AddressBook.actions";
import { Table, Card, Button, Space } from "antd";
import { Link, useHistory } from "react-router-dom";
import Avatar from "antd/lib/avatar/avatar";
import Header from "../components/Header";

const { Column } = Table;

const AddressBookList = ({
  addressBooks,
  refreshList,
  deleteAddressBook,
}) => {
  let history = useHistory();
  React.useEffect(() => {
    refreshList();
    console.log("hey");
  }, [refreshList]);
  return (
    <>
      <Header />
      <div
        className="container"
        style={{
          display: "flex",
          justifyContent: "center",
          backgroundColor: "lightgray",
          marginBottom: "100px",
        }}
      >
        <Card
          className="center-card card-shadow"
          title="ADDRESS BOOKS"
          style={{ width: "70%", textAlign: "center" }}
        >
          <Link to="/entry/create">
            <Button
              type="primary"
              style={{ float: "left", marginLeft: "15px" }}
            >
              Add
            </Button>
          </Link>
          <Table
            size="small"
            rowKey={(record) => record.id}
            dataSource={addressBooks}
            key={addressBooks.id}
            scroll={{ x: 240 }}
          >
            <Column
              hidden
              title="Profile Picture"
              dataIndex="image"
              key="image"
              render={(text, record) => <Avatar src={record.image} />}
            />
            <Column title="First Name" dataIndex="firstName" key="firstName" />
            <Column
              title="Middle Name"
              dataIndex="middleName"
              key="middleName"
            />
            <Column title="Last Name" dataIndex="lastName" key="lastName" />
            <Column title="Gender" dataIndex="gender" key="gender" />
            <Column title="Birthday" dataIndex="birthday" key="birthday" />
            <Column title="Age" dataIndex="age" key="age" />
            <Column
              title="Action"
              key="action"
              render={(text, record) => (
                <Space size="middle">
                  <button onClick={() => history.push("/entry/edit", record)}>
                    Edit
                  </button>
                  <button onClick={() => deleteAddressBook(record)}>
                    Delete
                  </button>
                </Space>
              )}
            />
          </Table>
        </Card>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  addressBooks: state.addressBooks.addressBooks,
});

const mapDispatchToProps = (dispatch) => ({
  refreshList: () => dispatch(refreshList),
  deleteAddressBook: (address) =>
    deleteAddressBook(address).then(dispatch(refreshList)),
  updateAddressBook: (addressBook) =>
    updateAddressBook(addressBook).then(dispatch(refreshList)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddressBookList);
