import React from "react";
import { connect } from "react-redux";
import { Table, Card } from "antd";
import { refreshList } from "./AddressBookReports.action";
import Header from "../../components/Header";
const { Column } = Table;

const AddressBookReportsList = ({ reports, refreshList }) => {
  React.useEffect(() => {
    refreshList();
  }, [refreshList]);
  return (
    <>
      <Header />
      <div
        className="container"
        style={{
          display: "flex",
          justifyContent: "center",
          backgroundColor: "lightgray",
          marginBottom: "100px",
        }}
      >
        <Card
          className="center-card card-shadow"
          title="Gender Reports"
          style={{ width: "70%", textAlign: "center" }}
        >
          <Table
            size="small"
            rowKey={(record) => record.id}
            dataSource={reports}
            key={reports.id}
            scroll={{ x: 240 }}
          >
            <Column title="Male" dataIndex="male" key="male" />
            <Column title="Female" dataIndex="female" key="female" />
            <Column title="Others" dataIndex="others" key="others" />
          </Table>
        </Card>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  reports: state.reports.reports,
});

const mapDispatchToProps = (dispatch) => ({
  refreshList: () => dispatch(refreshList),
  // addAddressBook: (addressBook)=> addAddressBook(addressBook).then(dispatch(refreshList)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddressBookReportsList);
