import {
  LIST_LOAD_REQUEST,
  LIST_LOAD_SUCCESS,
  LIST_LOAD_FAILURE,
} from "./AddressBookReports.types";
import { AddressBookServiceImpl } from "../../../../domain/usecases/AddressBookService";
import { AddressBookRepositoryFireBaseImpl } from "../../../../data/repositories/AddressBookRepositoryFireBaseImpl";

export const refreshList = async (dispatch) => {
  dispatch({ type: LIST_LOAD_REQUEST });

  try {
    const addressBookRepo = new AddressBookRepositoryFireBaseImpl();
    const addressBookService = new AddressBookServiceImpl(addressBookRepo);
    const addressBookReports = await addressBookService.GetAddressBookReports();
    console.log(addressBookReports);
    dispatch({ type: LIST_LOAD_SUCCESS, payload: addressBookReports });
  } catch (error) {
    dispatch({ type: LIST_LOAD_FAILURE, error });
  }
};
