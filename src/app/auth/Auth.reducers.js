import {
  SIGNIN_USER_SUCCESS,
  SIGNIN_USER_REQUEST,
  LOGOUT_USER_REQUEST,
  LOGOUT_USER_SUCCESS,
  LOGIN_USER_REQUEST,
  LOGIN_USER_ERROR,
  LOGIN_USER_SUCCESS,
  RESET_USER_REQUEST,
  RESET_USER_SUCCESS,
  CHANGE_USER_PASSWORD_ERROR,
  CHANGE_USER_PASSWORD_SUCCESS,
} from "./Auth.types";

const initialState = {
  authError: null,
  loading: false,
  match: true,
  signedIn: false,
  loggedIn: false,
  user: {},
  changePassMessage: null,
};

const authReducer = (state = initialState, action = null) => {
  switch (action.type) {
    case LOGIN_USER_ERROR:
      console.log("Login Failed");
      return {
        ...state,
        authError: "Login Failed",
        loading: false,
        match: false,
      };
    case LOGIN_USER_SUCCESS:
      console.log("Login Success!");
      return {
        ...state,
        authError: null,
        loading: false,
        match: true,
        loggedIn: true,
        user: action.payload,
      };
    case LOGOUT_USER_SUCCESS:
      console.log("signout Success");
      return {
        ...state,
        loading: false,
        match: true,
        loggedIn: false,
      };
    case LOGOUT_USER_REQUEST:
      console.log("Logout Request");
      return {
        ...state,
        loading: true,
        match: true,
      };
    case LOGIN_USER_REQUEST:
      console.log("Login Request");
      return {
        ...state,
        loading: true,
      };

    case SIGNIN_USER_REQUEST:
      console.log("Register Request");
      return {
        ...state,
        loading: true,
        match: true,
      };

    case SIGNIN_USER_SUCCESS:
      console.log("Register Success");
      return {
        ...state,
        loading: true,
        match: true,
        signedIn: true,
      };
    case RESET_USER_REQUEST:
      return {
        ...state,
        loading: true,
        match: true,
      };
    case RESET_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        match: true,
      };

    case CHANGE_USER_PASSWORD_ERROR:
      return {
        ...state,
        changePassMessage: action.payload,
      };
    case CHANGE_USER_PASSWORD_SUCCESS:
      return {
        ...state,
        changePassMessage: action.payload,
      };
    default:
      return state;
  }
};

export default authReducer;
