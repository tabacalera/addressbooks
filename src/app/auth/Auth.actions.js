import * as types from "./Auth.types";
import AuthServiceImpl from "../../domain/usecases/AuthService";
import AuthRepositoryFirebaseImpl from "../../data/repositories/AuthRepositoryFirebaseImpl";

export const signup = (data) => {
  return (dispatch) => {
    dispatch({ type: types.SIGNIN_USER_REQUEST });
    const AuthRepo = new AuthRepositoryFirebaseImpl();
    const AuthService = new AuthServiceImpl(AuthRepo);
    AuthService.Signup(data).then((response) => {
      dispatch({ type: types.SIGNIN_USER_SUCCESS, data });
    });
  };
};
export const login = (data) => {
  return async (dispatch) => {
    dispatch({ type: types.LOGIN_USER_REQUEST });
    const AuthRepo = new AuthRepositoryFirebaseImpl();
    const AuthService = new AuthServiceImpl(AuthRepo);
    let datum = AuthService.Login(data);
    datum.then(function (user) {
      if (user) {
        return dispatch({ type: types.LOGIN_USER_SUCCESS, payload:user });
      } else {
        return dispatch({ type: types.LOGIN_USER_ERROR });
      }
    });
  };
};
export const logout = (dispatch) => {
  const AuthRepo = new AuthRepositoryFirebaseImpl();
  const AuthService = new AuthServiceImpl(AuthRepo);
  AuthService.Logout().then((response) => {
    dispatch({ type: types.LOGOUT_USER_REQUEST });
  });
};

export const resetPassword = (data) => {
  return async (dispatch) => {
    dispatch({ type: types.RESET_USER_REQUEST });
    const AuthRepo = new AuthRepositoryFirebaseImpl();
    const authService = new AuthServiceImpl(AuthRepo);
    const message = authService.ResetPassword(data);
    message
      .then(function (err) {
        console.log(err);
      })
      .catch(function () {
        console.log("err");
      });
  };
};

export const changePassword = (data) => {
  return async (dispatch) => {
    // dispatch({ type: types.LOGIN_USER_REQUEST })
    const AuthRepo = new AuthRepositoryFirebaseImpl();
    const AuthService = new AuthServiceImpl(AuthRepo);
    let datum = AuthService.ChangePassword(data);
    await datum
      .then(function (user) {
        console.log(user);
        return dispatch({
          type: types.CHANGE_USER_PASSWORD_SUCCESS,
          payload: "Successfully Changed",
        });
      })
      .catch((err) => {
        console.log(err);
        return dispatch({
          type: types.CHANGE_USER_PASSWORD_ERROR,
          payload: err.message,
        });
      });
  };
};
