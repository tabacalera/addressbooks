import React from "react";
import { Form, Input, Button, Card } from "antd";
import { Link, useHistory } from "react-router-dom";
import { signup } from "./Auth.actions";
import { connect } from "react-redux";

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const RegisterPage = ({ signup, authReducer }) => {
  let history = useHistory();
  const [values, setValues] = React.useState({
    username: "",
    password: "",
    confirmpassword: "",
    status: "",
    errMessage: "",
  });

  const onFinish = (values) => {
    console.log("Success:", values);
    if (values.password !== values.confirmpassword) {
      setValues({
        ...values,
        status: "error",
        errMessage: "The Password do not match",
      });
    } else {
      setValues({
        ...values,
        status: "success",
        errMessage: "",
      });
      signup(values);
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };
  console.log(authReducer);
  if (authReducer.signedIn === true) {
    history.replace("/");
  }

  return (
    //   <Layout>

    //     <Content>
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        position: "relative",
        height: 100,
      }}
    >
      <Card
        hoverable
        className="center-card card-shadow"
        bordered={false}
        title="Sign Up"
        style={{ width: "50%" }}
      >
        <Form
          {...layout}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Email"
            name="username"
            rules={[
              {
                required: true,
                message: "Please input valid email!",
                type: "email",
              },
            ]}
          >
            <Input
              name="username"
              value={values.username}
              onChange={handleChange}
            />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
            validateStatus={values.status}
            hasFeedback
          >
            <Input.Password
              name="password"
              value={values.password}
              onChange={handleChange}
            />
          </Form.Item>
          <Form.Item
            label="Confirm Password"
            name="confirmpassword"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
            validateStatus={values.status}
            hasFeedback
            help={values.errMessage}
          >
            <Input.Password
              name="confirmpassword"
              value={values.confirmpassword}
              onChange={handleChange}
            />
          </Form.Item>
          <Form.Item>
            <Link to="/" style={{ float: "right" }}>
              Already a Member?
            </Link>
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit" style={{ float: "left" }}>
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
};

const mapStateToProps = (state) => ({
  authReducer: state.authReducer,
});
const mapDispatchToProps = (dispatch) => ({
  signup: (username, password) => dispatch(signup(username, password)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage);
