import React from "react";
import { Form, Input, Button, Card } from "antd";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { resetPassword } from "./Auth.actions";
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const ResetPage = ({ resetPassword }) => {
  let history = useHistory();
  const [email, setEmail] = React.useState("");
  const onFinish = (values) => {
    console.log("Success:", values);
    resetPassword(values.username);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    //   <Layout>

    //     <Content>
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        position: "relative",
        height: 100,
      }}
    >
      <Card
        extra={<a onClick={() => history.goBack()}>Go Back</a>}
        hoverable
        className="center-card card-shadow"
        bordered={false}
        title="Reset Password"
        style={{ width: "50%" }}
      >
        <Form
          {...layout}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Username"
            name="username"
            rules={[
              {
                required: true,
                message: "please input a valid email",
                type: "email",
              },
            ]}
            hasFeedback
          >
            <Input
              name="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
};
const mapStateToProps = (state) => ({
  authReducer: state.authReducer,
});
const mapDispatchToProps = (dispatch) => ({
  resetPassword: (email) => dispatch(resetPassword(email)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ResetPage);
