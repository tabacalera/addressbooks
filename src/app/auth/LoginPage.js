import React from "react";
import { Form, Input, Button, Card } from "antd";
import { Link, useHistory } from "react-router-dom";
import { login } from "./Auth.actions";
import { connect } from "react-redux";
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const LoginPage = ({ login, authReducer }) => {
  let history = useHistory();
  const [values, setValues] = React.useState({
    username: "",
    password: "",
  });
  const onFinish = (values) => {
    console.log("Success:", values);

    console.log(login(values));
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };
  if (authReducer.loggedIn) {
    history.replace("/entry");
  }
  console.log(authReducer);
  return (
    //   <Layout>

    //     <Content>
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        position: "relative",
        height: 100,
      }}
    >
      <Card
        hoverable
        className="center-card card-shadow"
        bordered={false}
        title="Login"
        style={{ width: "50%" }}
      >
        <Form
          {...layout}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Username"
            name="username"
            rules={[
              {
                required: true,
                message: "Please input valid email!",
                type: "email",
              },
            ]}
          >
            <Input
              name="username"
              value={values.username}
              onChange={handleChange}
            />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password
              name="password"
              value={values.password}
              onChange={handleChange}
            />
          </Form.Item>
          <Form.Item>
            <Link to="/reset" style={{ float: "right" }}>
              Forgot password?
            </Link>
            <Link to="/register" style={{ float: "right", marginRight: 20 }}>
              Register
            </Link>
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
            <span style={{ color: "red" }}>{authReducer.authError}</span>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
};

const mapStateToProps = (state) => ({
  authReducer: state.authReducer,
});
const mapDispatchToProps = (dispatch) => ({
  login: (username, password) => dispatch(login(username, password)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
