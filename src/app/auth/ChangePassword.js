import React from "react";
import { Form, Input, Button, Card } from "antd";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { changePassword } from "./Auth.actions";
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const ChangePassword = ({ changePassword, authReducer }) => {
  let history = useHistory();
  const [values, setValues] = React.useState({
    username: authReducer.user.email,
    password: "",
    newPassword: "",
    confirmNewPassword: "",
  });
  const onFinish = () => {
    console.log("Success:", values);
    if (values.newPassword === values.confirmNewPassword) {
      changePassword(values);
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };
  return (
    //   <Layout>

    //     <Content>
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        position: "relative",
        height: 100,
      }}
    >
      <Card
        extra={<a onClick={() => history.goBack()}>Go Back</a>}
        hoverable
        className="center-card card-shadow"
        bordered={false}
        title="Change Password"
        style={{ width: "60%" }}
      >
        <Form
          {...layout}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Old Password"
            name="password"
            rules={[
              {
                required: true,
                message: "please input your old password",
              },
            ]}
            hasFeedback
          >
            <Input.Password
              name="password"
              value={values.password}
              onChange={handleChange}
            />
          </Form.Item>
          <Form.Item
            label="New Password"
            name="newPassword"
            rules={[
              {
                required: true,
                message: "please input your new password",
              },
            ]}
            hasFeedback
          >
            <Input.Password
              name="newPassword"
              value={values.newPassword}
              onChange={handleChange}
            />
          </Form.Item>
          <Form.Item
            label="Confirm Password"
            name="confirmNewPassword"
            rules={[
              {
                required: true,
                message: "please input a valid email",
              },
            ]}
            hasFeedback
          >
            <Input.Password
              name="confirmNewPassword"
              value={values.confirmNewPassword}
              onChange={handleChange}
            />
          </Form.Item>

          <code style={{ color: "red" }}>{authReducer.changePassMessage}</code>
          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
};
const mapStateToProps = (state) => ({
  authReducer: state.authReducer,
});
const mapDispatchToProps = (dispatch) => ({
  changePassword: (user) => dispatch(changePassword(user)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);
