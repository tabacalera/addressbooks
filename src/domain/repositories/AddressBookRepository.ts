import { AddressBook, AddressBookReport } from "../entities/AddressBook";

export interface AddressBookRepository {
  GetAddressBook(): Promise<AddressBook[]>;
  GetAddressBookReports(): Promise<AddressBookReport[]>;
  AddAddressBook(data:AddressBook): void;
  DeleteAddressBook(data:AddressBook): void;
  UpdateAddressBook(data:AddressBook): void;
}
