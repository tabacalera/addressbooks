import { User, UserReset } from "../entities/User"

export interface AuthRepository {
    Signup(data:User): void;
    ResetPassword(data:UserReset): void;
    ChangePassword(data:User): Promise<object>;
    Login(data:User): Promise<object> ;
    Logout(): void;
  }