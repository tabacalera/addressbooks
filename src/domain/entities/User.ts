export class User {
    username: string;
    password: string;

    constructor(username: string, password: string) {
    this.username = username;
    this.password = password;
    }
}

export class UserReset {
    email: string;

    constructor(email: string) {
        this.email = email;
    }
}

export class currentUser {
    user: object;

    constructor(user:object){
        this.user = user
    }
}

export class changePassUser {
    username: string;
    password: string;
    newPassword: string;
    constructor(username:string, password: string, newPassword:string){
        this.username = username
        this.password = password
        this.newPassword = newPassword
    }
}