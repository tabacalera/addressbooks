export class AddressBook {
    id: number;
    firstName: string;
    lastName: string;
    middleName: string;
    birthday: string;
    age: number;
    gender: string;
    image: File;
  
    constructor( id: number, firstName: string,lastName: string,middleName: string,birthday: string,age: number, gender:string, image:File) {
      this.id = id;
      this.firstName = firstName;
      this.lastName = lastName;
      this.middleName = middleName;
      this.birthday = birthday;
      this.age = age;
      this.gender = gender;
      this.image=image
      
    }
  }

export class AddressBookReport {
  male: number;
  female: number;
  others: number;

  constructor(male: number, female: number, others: number) {
    this.male = male;
    this.female = female;
    this.others = others;
  }
}
