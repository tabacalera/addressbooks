import { AddressBook, AddressBookReport } from "../entities/AddressBook";
import { AddressBookRepository } from "../repositories/AddressBookRepository";

export interface AddressBookService {
    GetAddressBook(): Promise<AddressBook[]>;
    AddAddressBook(data:AddressBook): void;
    UpdateAddressBook(data:AddressBook): void;
    
}
export class AddressBookServiceImpl implements AddressBookService {
  itemRepo: AddressBookRepository;

  constructor(ir: AddressBookRepository) {
    this.itemRepo = ir;
  }
  async GetAddressBookReports(): Promise<AddressBookReport[]> {
    return this.itemRepo.GetAddressBookReports()
  }
  async GetAddressBook(): Promise<AddressBook[]> {
    return this.itemRepo.GetAddressBook();
  }
  async AddAddressBook(data:AddressBook) {
    const today = new Date();
    const birthDate = new Date(data.birthday)
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    data.age = age
    
    return this.itemRepo.AddAddressBook(data)
    // this.itemRepo.AddAddressBook(data)
  }
  async DeleteAddressBook(data:AddressBook) {
    return this.itemRepo.DeleteAddressBook(data)
  }

  async UpdateAddressBook(data:AddressBook){
    console.log(data);
    
    const today = new Date();
    const birthDate = new Date(data.birthday)
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    data.age = age
    return this.itemRepo.UpdateAddressBook(data)
  }
}
