import { User, UserReset } from "../entities/User"
import { AuthRepository } from "../repositories/AuthRepository"

export interface AuthService {
    Signup(data:User): void;
    Login(data:User): Promise<object> ;
    Logout(): void;
}

export default class AuthServiceImpl implements AuthService {
    authRepo: AuthRepository;

    constructor(ir: AuthRepository) {
        this.authRepo= ir
    }

    async Signup(data: User){
        this.authRepo.Signup(data)
    }
    async Login(data:User): Promise<object> {
        if(data){
            return this.authRepo.Login(data)
        }
        else {
            throw "Error"
        }
    }
    async Logout(){
        return this.authRepo.Logout()
    }
    async ResetPassword(data:UserReset) {
        return this.authRepo.ResetPassword(data)
    }
    async ChangePassword(data:User): Promise<object> {
        if(data){
            return this.authRepo.ChangePassword(data)
        }
        else {
            throw "Error"
        }
    }
}